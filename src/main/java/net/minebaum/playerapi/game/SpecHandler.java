package net.minebaum.playerapi.game;

import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.developement.GuiAPI;
import net.minebaum.playerapi.developement.ItemBuilder;
import net.minebaum.playerapi.developement.Skull;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.ArrayList;

public class SpecHandler {

    private ArrayList<MBPlayer> players;
    private ItemStack[] invItems;
    private Game game;
    private ISetinventory method;

    public SpecHandler(final ArrayList<MBPlayer> specedPlayers, final Game game, final ISetinventory method){
        this.players = specedPlayers;
        this.game = game;
        this.method = method;
    }

    public void setInvItems(ItemStack[] invItems) {
        this.invItems = invItems;
    }

    public void setMethod(ISetinventory method) {
        this.method = method;
    }

    public ArrayList<MBPlayer> getPlayers() {
        return players;
    }

    public Game getGame() {
        return game;
    }

    public void setPlayers(final ArrayList<MBPlayer> players) {
        this.players = players;
    }

    public SpecHandler setSpec(final MBPlayer player){
        if(players.contains(player)){
            player.getSpigotPlayer().getInventory().clear();
        }else{
            player.getSpigotPlayer().getInventory().clear();
            for(int i = 0;i < 36;i++){
                player.getSpigotPlayer().getInventory().setItem(i, invItems[i]);
            }
        }
        return this;
    }

    public boolean isCheckModeOnClicked(final ItemStack is){
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§cCheckMode §8× §aAn")){
            return true;
        }else return false;
    }

    public boolean isCheckModeOffClicked(final ItemStack is){
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§cCheckMode §8× §cAus")){
            return true;
        }else return false;
    }

    public void update(){
        for(MBPlayer all : game.getPlayers()){
            for(MBPlayer specs : players){
                all.getSpigotPlayer().hidePlayer(specs.getSpigotPlayer());
            }
        }
    }

    public Inventory navInv(){
        Inventory inv = new GuiAPI().GUI(54, "§cOnlinespieler");

        for(MBPlayer all : game.getPlayers()){
            for(MBPlayer specs : players){
                if(specs == all){ }else{
                    ItemStack item = Skull.getPlayerSkull(all.getName());
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName(all.getName());
                    item.setItemMeta(meta);
                    inv.addItem(item);
                }
            }
        }

        return inv;
    }

    public Inventory confInv(final Player player){
        Inventory inv = new GuiAPI().fillerGUI(9, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 15).setDisplayname(" ").build(), "§cEinstellungen");
        if(players.contains(player)){
            inv.setItem(4, new ItemBuilder(Material.REDSTONE, 1, (short) 0).setDisplayname("§cCheckMode §8× §aAn").build());
        }else
            inv.setItem(4, new ItemBuilder(Material.REDSTONE, 1, (short) 0).setDisplayname("§cCheckMode §8× §cAus").build());
        return inv;
    }

}
