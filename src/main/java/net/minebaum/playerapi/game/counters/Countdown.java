package net.minebaum.playerapi.game.counters;

public abstract class Countdown {

    protected int taskID;
    protected boolean isRunning;

    public abstract void start();
    public abstract void stop();

    public int getTaskID() {
        return taskID;
    }

    public boolean isRunning() {
        return isRunning;
    }
}
