package net.minebaum.playerapi.game.states;

public abstract class GameState {

    protected int ID;
    
    public abstract void start();
    public abstract void stop();
    
}
