package net.minebaum.playerapi.game.team;

import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.MBProxyPlayer;
import net.minebaum.playerapi.events.PropertieAddEvent;
import net.minebaum.playerapi.game.Game;
import net.minebaum.playerapi.properties.Propertie;
import org.bukkit.Bukkit;

import java.util.ArrayList;

public class Team {

    private ArrayList<MBPlayer> inTeam;
    private String color;
    private String name;
    private int ID;
    private Game game;
    private Propertie[] properties;
    private byte propertiesLength;

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public byte getPropertiesLength() {
        return propertiesLength;
    }

    public void setPropertiesLength(byte propertiesLength) {
        this.propertiesLength = propertiesLength;
    }

    public Team(Game game, String name, String color, int ID, byte propertiesLength){
        this.color = color;
        this.ID = ID;
        this.game = game;
        this.name = name;
        this.propertiesLength = propertiesLength;
    }

    public Team addPlayer(MBPlayer player){
        if(inTeam.contains(player)) {return this;}
        else
        {
            inTeam.add(player);
        }
        return this;
    }

    public Team removePlayer(MBPlayer player){
        if(!inTeam.contains(player)) {return this;}
        else
        {
            inTeam.remove(player);
        }
        return this;
    }

    public boolean isInTeam(MBPlayer player){
        return inTeam.contains(player);
    }

    public Game getGame() {
        return game;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getID() {
        return ID;
    }

    public Team addPropertie(Propertie a){
        Bukkit.getPluginManager().callEvent(new PropertieAddEvent(a));
        if(propertiesLength > properties.length)
            properties[properties.length] = a;
        return this;
    }

    public boolean hasPropertie(Propertie a){
        for(Propertie propertie : properties){
            if(propertie.equals(a)) return true;
        }
        return false;
    }

    public Team removePropertie(Propertie a) throws Exception{
        int i = 0;
        for(Propertie propertie : properties){
            if(a.equals(propertie)){
                properties[i] = null;
            }
            i++;
        }
        return this;
    }
}
