package net.minebaum.playerapi.game;

import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.game.states.GameState;
import net.minebaum.playerapi.game.team.Team;
import net.minebaum.playerapi.mysql.MySQLConnector;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;

public class Game {

    private String name;
    private MySQLConnector mySQLConnector;
    private int minPlayers;
    private int maxPlayers;
    private ArrayList<MBPlayer> specplayers;
    private ArrayList<MBPlayer> players;
    private ArrayList<Team> teams;
    private Plugin plugin;
    private GameState[] gameStates;
    private SpecHandler specHandler;
    private GameState currentGameState;
    private ISetinventory[] inventorySetter;

    public GameState getCurrentGameState() {
        return currentGameState;
    }
    public String getName() {
        return name;
    }
    public Plugin getPlugin() {
        return plugin;
    }
    public GameState[] getGameStates() {
        return gameStates;
    }
    public ArrayList<MBPlayer> getPlayers() {
        return players;
    }
    public ArrayList<MBPlayer> getSpecplayers() {
        return specplayers;
    }
    public ArrayList<Team> getTeams() {
        return teams;
    }
    public int getMaxPlayers() {
        return maxPlayers;
    }
    public int getMinPlayers() {
        return minPlayers;
    }
    public MySQLConnector getMySQLConnector() {
        return mySQLConnector;
    }
    public SpecHandler getSpecHandler() {
        return specHandler;
    }

    public Game(String name, MySQLConnector connector, int minPlayers, int maxPlayers, ArrayList<Team> teams, Plugin plugin, ISetinventory setSpecInv){
        this.maxPlayers = maxPlayers;
        this.minPlayers = minPlayers;
        this.mySQLConnector = connector;
        this.name = name;
        this.teams = teams;
        this.plugin = plugin;
        specHandler = new SpecHandler(getSpecplayers(), this, setSpecInv);
    }

    public Game setInventorySetter(ISetinventory[] setters){
        this.inventorySetter = setters;
        return this;
    }

    public Game setupGameStates(GameState[] gameStates, GameState currentGameState){
        this.gameStates = gameStates;
        this.currentGameState = currentGameState;
        return this;
    }

    public Game addPlayer(MBPlayer player){
        if(players.contains(player)) {return this;}
        else{
            players.add(player);
            return this;
        }
    }

    public Game removePlayer(MBPlayer player){
        if(!players.contains(player)) {return this;}
        else{
            players.remove(player);
            return this;
        }
    }
}
