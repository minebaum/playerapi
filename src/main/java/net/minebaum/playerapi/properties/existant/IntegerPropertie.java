package net.minebaum.playerapi.properties.existant;

import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.properties.Propertie;

public class IntegerPropertie extends Propertie {

    //FIELDS
    private MBPlayer owner;
    private Object obj;


    public IntegerPropertie(MBPlayer owner, Object obj){
        this.owner = owner;
        this.obj = obj;
    }

    //GETTER
    @Override
    public Propertie getPropertie() {
        return this;
    }

    public MBPlayer getOwner() {
        return owner;
    }

    public Object getObject() {
        return obj;
    }

    public void setOwner(MBPlayer owner) {
        this.owner = owner;
    }
    @Override
    public PropertieType getPropertieType() {
        return PropertieType.INT;
    }

    @Override
    public MBPlayer getPropertieOwner() {
        return null;
    }

    //SETTER
    public void setObject(Object obj) {
        this.obj = obj;
    }
}
