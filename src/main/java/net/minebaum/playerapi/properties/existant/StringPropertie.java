package net.minebaum.playerapi.properties.existant;

import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.properties.Propertie;

public class StringPropertie extends Propertie {

    private MBPlayer owner;
    private Object obj;

    public Object getObject() {
        return obj;
    }

    public void setObject(Object obj) {
        this.obj = obj;
    }

    public StringPropertie(Object a, MBPlayer owner){
        this.owner = owner;
        obj = a;
    }

    public void setOwner(MBPlayer owner) {
        this.owner = owner;
    }

    public MBPlayer getOwner() {
        return owner;
    }

    @Override
    public Propertie getPropertie() {
        return this;
    }

    @Override
    public PropertieType getPropertieType() {
        return PropertieType.TEXT;
    }

    @Override
    public MBPlayer getPropertieOwner() {
        return null;
    }
}
