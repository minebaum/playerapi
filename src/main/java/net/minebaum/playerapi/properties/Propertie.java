package net.minebaum.playerapi.properties;

import net.minebaum.playerapi.MBPlayer;

public abstract class Propertie {

    public static int TEMP = 0,
                    MYSQL = 1,
                    CPROPERT = 2,
                    FILE = 3;

    public enum PropertieType{
        TEXT, INT, FILE;
    }

    public abstract Propertie getPropertie();
    public abstract PropertieType getPropertieType();
    public abstract MBPlayer getPropertieOwner();


}
