package net.minebaum.playerapi.properties;

import net.minebaum.playerapi.properties.existant.IntegerPropertie;
import net.minebaum.playerapi.properties.existant.StringPropertie;

public class PropertieHandler {

    public StringPropertie stringPropertiefromNullOwner(Object a){
        return new StringPropertie(a, null);
    }

    public IntegerPropertie integerPropertiefromNullOwner(Object a){
        return new IntegerPropertie(null, a);
    }

}
