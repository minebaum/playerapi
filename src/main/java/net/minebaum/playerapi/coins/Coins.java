package net.minebaum.playerapi.coins;

import net.minebaum.playerapi.events.MoneySetEvent;
import net.minebaum.playerapi.mysql.MySQLConnector;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.ResultSet;

public class Coins {

    private MySQLConnector mySQL;
    private Plugin plugin;
    private int defaultcoins;
    private String apiName;

    public int getDefaultcoins() {
        return defaultcoins;
    }

    public void setDefaultcoins(int defaultcoins) {
        this.defaultcoins = defaultcoins;
    }

    public Coins(Plugin plugin, String apiName){
        this.apiName = apiName;
        this.plugin = plugin;
    }

    public String getApiName() {
        return apiName;
    }

    public MySQLConnector getMySQL() {
        return mySQL;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public Coins setupMySQL(MySQLConnector connector){
        mySQL = connector;
        mySQL.update("CREATE TABLE IF NOT EXISTS coins (UUID VARCHAR(50), Coins INTEGER)");
        return this;
    }
    public Coins setup(int defaultcoins){
        setDefaultcoins(defaultcoins);
        return this;
    }
    
    public void createAccount(Player p){
        boolean exist = false;
        try {
            ResultSet rs =
                    mySQL.query("SELECT Coins FROM coins WHERE UUID='" +
                            p.getUniqueId().toString() + "';");
            while (rs.next())
                exist = Boolean.valueOf(true).booleanValue();
        } catch (Exception err) {
            System.err.println(err);
        }
        if (!exist) {
            mySQL.update("INSERT INTO coins (UUID,Coins) values ('" +
                    p.getUniqueId().toString() + "', " + this.defaultcoins + ");");}
    }

    public int getCoins(Player player){
        int current = 0;
        try {
            ResultSet rs =
                    mySQL.query("SELECT Coins FROM coins WHERE UUID='" +
                            player.getUniqueId().toString() + "';");
            while (rs.next())
                current = rs.getInt(1);
        } catch (Exception err) {
            System.err.println(err);
        }
        return current;
    }

    public void setCoins(Player player, int money){
        MoneySetEvent event = new MoneySetEvent(money, player, apiName, this);
        Bukkit.getPluginManager().callEvent(event);
        if(event.isCancelled()){
            System.err.println("[API] Transition Cancelled cause by event.");
            return;
        }
        mySQL.update("UPDATE coins SET Coins = " + money + " WHERE UUID='" + player.getUniqueId().toString() + "';");
        System.out.println("Coins of Player " + player.getName() + " were setted to " + money);
    }

    public void addCoins(Player player, int money){
        if(money <= 0){
            return;
        }
        int currentmoney = getCoins(player);
        setCoins(player, currentmoney + money);
    }

    public void removeCoins(Player player, int money){
        if(money >= getCoins(player)){
            setCoins(player, 0);
        }else
        {
            setCoins(player, getCoins(player) - money);
        }
    }

}
