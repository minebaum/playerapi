package net.minebaum.playerapi;

import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.player.ICloudPlayer;
import eu.thesimplecloud.api.player.text.CloudText;
import eu.thesimplecloud.module.permission.PermissionPool;
import eu.thesimplecloud.module.permission.player.IPermissionPlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.minebaum.playerapi.events.PlayerSendToServerEvent;
import net.minebaum.playerapi.events.PropertieAddEvent;
import net.minebaum.playerapi.events.ProxyPlayerSendToServerEvent;
import net.minebaum.playerapi.properties.Propertie;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.permissions.Permission;

import java.util.Objects;

public class MBProxyPlayer {

    //NORM ATTRIBUTES
    private String name;
    private ICloudPlayer cloudPlayer;
    private IPermissionPlayer permissionPlayer;
/* PROXY ONLY*/private ProxiedPlayer player;
    private Propertie[] properties;
    private byte propertiesLength;

    //GETTER
    public Propertie[] getProperties() {
        return properties;
    }
    public byte getPropertiesLength() {
        return propertiesLength;
    }
    public String getName() {
        return name;
    }
    public IPermissionPlayer getPermissionPlayer() {
        return permissionPlayer;
    }
    public ICloudPlayer getCloudPlayer() {
        return cloudPlayer;
    }
    public ProxiedPlayer getPlayer() {
        return player;
    }

    //CONSTRUCTOR
    public MBProxyPlayer(ProxiedPlayer player){
        this.player = player;
        this.name = player.getName();
        this.cloudPlayer = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(player.getUniqueId());
        this.permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(player.getUniqueId());
    }


    //METHODS
    public MBProxyPlayer addPropertie(Propertie a){
        Bukkit.getPluginManager().callEvent(new PropertieAddEvent(a));
        if(propertiesLength > properties.length)
            properties[properties.length] = a;
        return this;
    }

    public boolean hasPropertie(Propertie a){
        for(Propertie propertie : properties){
            if(propertie.equals(a)) return true;
        }
        return false;
    }

    public MBProxyPlayer removePropertie(Propertie a) throws Exception{
        int i = 0;
        for(Propertie propertie : properties){
            if(a.equals(propertie)){
                properties[i] = null;
            }
            i++;
        }
        return this;
    }

    public enum MessageType{
        CHAT, ACTIONBAR, TITLE;
    }

    public MBProxyPlayer send(String value, String value2, MBPlayer.MessageType type){
        value = ChatColor.translateAlternateColorCodes('&', value);
        value2 = ChatColor.translateAlternateColorCodes('&', value2);
        if(type.equals(MBPlayer.MessageType.CHAT)){
            cloudPlayer.sendMessage(new CloudText(value));
        }else if(type.equals(MBPlayer.MessageType.TITLE)){
            cloudPlayer.sendTitle(Objects.requireNonNull(value), Objects.requireNonNull(value2), 5, 20, 5);
        }else if(type.equals(MBPlayer.MessageType.ACTIONBAR)){
            String message = value;
            if(message == null) message = "";
            cloudPlayer.sendActionBar(message.replaceAll("&", "§"));
        }
        return this;
    }

    public MBProxyPlayer sendMessage(String value){
        cloudPlayer.sendMessage(new CloudText(value));
        return this;
    }

    public MBProxyPlayer sendActionbar(String value){
        send(value, null, MBPlayer.MessageType.ACTIONBAR);
        return this;
    }

    public MBProxyPlayer sendTitle(String value, String value2){
        send(value, value2, MBPlayer.MessageType.TITLE);
        return this;
    }

    public MBProxyPlayer sendToServer(String serviceName){
        ProxyPlayerSendToServerEvent e = new ProxyPlayerSendToServerEvent(this);
        Bukkit.getPluginManager().callEvent(e);
        if(e.isCancelled()){
            return this;
        }else
            cloudPlayer.connect(Objects.requireNonNull(CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName(serviceName)));
        return this;
    }

}
