package net.minebaum.playerapi.language;

public enum Language {

    GERMAN, ENGLISH, FRENCH;

}
