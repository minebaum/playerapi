package net.minebaum.playerapi.language.messages;

import lombok.Getter;
import net.minebaum.playerapi.language.Language;

import java.util.Objects;

@Getter
public class RawMessage {

    private String key;
    private String[] messages;

    public RawMessage(String key, String[] messages){
        this.key = key;
        this.messages = messages;
    }

    public String getMessageBYID(int ID){
        return Objects.requireNonNull(messages[ID]);
    }

    public int languageToID(Language language){
        int ID = 0;
        for(Language l : Language.values()){
            if(l == language){
                return ID;
            }
            ID++;
        }
        return -1;
    }

}
