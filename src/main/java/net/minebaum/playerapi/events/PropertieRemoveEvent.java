package net.minebaum.playerapi.events;

import net.minebaum.playerapi.properties.Propertie;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PropertieRemoveEvent extends Event {

    private HandlerList handlers = new HandlerList();
    private Propertie propertie;
    private boolean cancelled;

    public PropertieRemoveEvent(Propertie propertie){
        this.propertie = propertie;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public Propertie getPropertie() {
        return propertie;
    }

    public void setPropertie(Propertie propertie) {
        this.propertie = propertie;
    }

    @Override
    public HandlerList getHandlers() {
        return this.handlers;
    }

    public HandlerList getHandlerList(){
        return handlers;
    }

}
