package net.minebaum.playerapi.events;

import net.minebaum.playerapi.coins.Coins;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MoneySetEvent extends Event {

    private HandlerList handlers = new HandlerList();
    private int a;
    private Player b;
    private String c;
    private Coins api;
    private boolean cancelled;

    public MoneySetEvent(int a, Player b, String c, Coins api){
        this.a = a;
        this.b = b;
        this.c = c;
        this.api = api;
    }

    public MoneySetEvent(int a, Player b, Coins api){
        this.a = a;
        this.b = b;
        this.api = api;
        this.c = api.getApiName();
    }

    public int getMoney() {
        return a;
    }

    public Player getPlayer() {
        return b;
    }

    public String getCoinsAPIName() {
        return c;
    }

    public Coins getApi() {
        return api;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return this.handlers;
    }

    public HandlerList getHandlerList(){
        return handlers;
    }

}
