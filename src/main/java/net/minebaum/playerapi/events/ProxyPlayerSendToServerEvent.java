package net.minebaum.playerapi.events;

import net.minebaum.playerapi.MBProxyPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ProxyPlayerSendToServerEvent extends Event {

    private HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private MBProxyPlayer player;

    public MBProxyPlayer getPlayer() {
        return player;
    }

    public ProxyPlayerSendToServerEvent(MBProxyPlayer player){
        this.player = player;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return this.handlers;
    }

    public HandlerList getHandlerList(){
        return handlers;
    }

}
