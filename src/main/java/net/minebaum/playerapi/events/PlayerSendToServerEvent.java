package net.minebaum.playerapi.events;

import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.properties.Propertie;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerSendToServerEvent extends Event {

    private HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private MBPlayer player;

    public MBPlayer getPlayer() {
        return player;
    }

    public PlayerSendToServerEvent(MBPlayer player){
        this.player = player;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return this.handlers;
    }

    public HandlerList getHandlerList(){
        return handlers;
    }


}
