package net.minebaum.playerapi.developement.logger;

import com.sun.xml.internal.fastinfoset.util.StringArray;

public class Log {

    private String[] log;

    public Log(){
        this.log = new String[StringArray.MAXIMUM_CAPACITY];
    }

    public Log log(String log){
        this.log[log.length()] = log;
        return this; }

}
