package net.minebaum.playerapi.developement;

public class Data {
    public static final String PREFIX = "§c§lMineBaum §8✖ §7";
    public static final String NOPERMS = PREFIX + "§cDiesen Befehl gibt es nicht oder du bist nicht berechtigt ihn auszuführen.";
    public static String FALSE_USAGE(String rightUsage){
        return Data.PREFIX + "§cNutze: " + rightUsage;
    }
}
