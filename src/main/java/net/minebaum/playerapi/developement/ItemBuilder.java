package net.minebaum.playerapi.developement;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemBuilder {

    private ItemStack itemStack;
    private ItemMeta meta;

    public ItemBuilder(Material material, int amount, short s) {
        this.itemStack = new ItemStack(material,amount,s);
        this.meta = this.itemStack.getItemMeta();
    }
    public ItemBuilder setDisplayname(String name) {
        this.meta.setDisplayName(name);
        return this;
    }
    public ItemBuilder setUnbreakable() {
        this.meta.spigot().setUnbreakable(true);
        return this;
    }
    public ItemBuilder addEnchantment(Enchantment e, Integer strenght) {
        this.meta.addEnchant(e,strenght,true);
        return this;
    }
    public ItemBuilder addItemFlag(ItemFlag itemFlag) {
        this.meta.addItemFlags(itemFlag);
        return this;
    }
    public ItemBuilder setLore(List<String> lore) {
        this.meta.setLore(lore);
        return this;
    }
    public ItemStack build() {
        this.itemStack.setItemMeta(meta);
        return this.itemStack;
    }
}

