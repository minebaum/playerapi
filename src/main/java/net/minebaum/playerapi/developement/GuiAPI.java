package net.minebaum.playerapi.developement;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GuiAPI {

    public GuiAPI() {
        super();
    }

    public Inventory fillerGUI(int size, ItemStack filler, String title) {
        Inventory i = Bukkit.createInventory(null, size, title);
        for(int in = 0;in < size;in++) {
            i.setItem(in, filler);
        }
        return i;
    }

    public Inventory fillerGUI(InventoryType inventtype, ItemStack filler, String title) {
        Inventory i = Bukkit.createInventory(null, inventtype, title);
        int size = 0;
        if(inventtype == InventoryType.WORKBENCH) {
            size = 7;
        }else if(inventtype == InventoryType.ANVIL) {
            size = 3;
        }else if(inventtype == InventoryType.CHEST) {
            size = 27;
        }else if(inventtype == InventoryType.BREWING) {
            size = 4;
        }else if(inventtype == InventoryType.CRAFTING) {
            size = 4;
        }else if(inventtype == InventoryType.CREATIVE) {
            size = 0;
        }else if(inventtype == InventoryType.DISPENSER) {
            size = 9;
        }else if(inventtype == InventoryType.DROPPER) {
            size = 9;
        }else if(inventtype == InventoryType.ENCHANTING) {
            size = 2;
        }else if(inventtype == InventoryType.ENDER_CHEST) {
            size = 27;
        }else if(inventtype == InventoryType.FURNACE) {
            size = 3;
        }else if(inventtype == InventoryType.HOPPER) {
            size = 5;
        }else if(inventtype == InventoryType.MERCHANT) {
            size = 2;
        }else if(inventtype == InventoryType.PLAYER) {
            size = 0;
        }
        for(int in = 0;in < size;in++) {
            i.setItem(in, filler);
        }
        return i;
    }

    public Inventory GUI(int size, String title) {
        Inventory i = Bukkit.createInventory(null, size, title);
        return i;
    }

    public Inventory GUI(InventoryType inventtype, String title) {
        Inventory i = Bukkit.createInventory(null, inventtype, title);
        return i;
    }

}

