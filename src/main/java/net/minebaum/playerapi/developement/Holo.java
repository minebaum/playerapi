package net.minebaum.playerapi.developement;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Holo {

    private HashMap<String, ArmorStand> holos = new HashMap<String, ArmorStand>();
    private List<Player> deleting = new ArrayList<Player>();
    private HashMap<Player, List<ArmorStand>> undo = new HashMap<Player, List<ArmorStand>>();

    public void create(Location l, String msg, String name) {
        ArmorStand holo = (ArmorStand) l.getWorld().spawnEntity(l, EntityType.ARMOR_STAND);
        holo.setVisible(false);
        holo.setCustomName(msg);
        holo.setCustomNameVisible(true);
        holo.setGravity(false);

        if(name != null) {
            holos.put(name, holo);
            return;
        }
        holos.put(msg, holo);
    }

    public void delete(Player p, Entity e) {
        if(e.getType() != EntityType.ARMOR_STAND) return;

        e.remove();
        if(getUndo().containsKey(p)) {
            getUndo().get(p).add((ArmorStand)e);
        } else {
            List<ArmorStand> armList = new ArrayList<ArmorStand>();
            armList.add((ArmorStand)e);
            getUndo().put(p, armList);
        }
        p.sendMessage("Succesfully removed Armorstand: " + e.getCustomName());
    }

    public void deleteByExactName(String name) {
        for(World allW : Bukkit.getWorlds()) {
            for(Entity allE : allW.getEntities()) {
                if(allE.getType() == EntityType.ARMOR_STAND && allE.getCustomName().equals(name)) allE.remove();
            }
        }
    }

    public List<Player> getDeleting() {
        return deleting;
    }

    public HashMap<Player, List<ArmorStand>> getUndo() {
        return undo;
    }
}

