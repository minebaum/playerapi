package net.minebaum.playerapi.developement;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ServerManager {

    private Plugin plugin;
    private String pluginName;
    private List<String> log;

    public ServerManager(Plugin plugin, String pluginName){
        this.plugin = plugin;
        this.pluginName = pluginName;
        log = new ArrayList<String>();
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public String getPluginName() {
        return pluginName;
    }

    public ServerManager LOG(String current){
        plugin.getServer().getConsoleSender().sendMessage("[" + this.pluginName.toUpperCase(Locale.ROOT) + "] " + current.replaceAll("&", "§"));
        log.add(current);
        return this;
    }

    public ServerManager LOGERR(String err){
        plugin.getServer().getConsoleSender().sendMessage("[" + this.pluginName.toUpperCase(Locale.ROOT) + " §cERROR§r] " + err.replaceAll("&", "§"));
        log.add("§r[§cERROR§r] " + err);
        return this;
    }

    public ServerManager LOGWARN(String current){
        plugin.getServer().getConsoleSender().sendMessage("[" + this.pluginName.toUpperCase(Locale.ROOT) + " §eWARNING§r] " + current.replaceAll("&", "§"));
        log.add("§r[§eWARNING§r] " + current);
        return this;
    }

    public List<String> getLog() {
        return log;
    }

    public ServerManager printLOG() throws IOException {
        File file = new File("plugins//" + this.pluginName + "//logs//apiLog-LATEST.yml");
        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        configuration.set("", log);
        configuration.save(file);
        return this;
    }

    public String getServerVersion(){
        return this.plugin.getServer().getVersion().split("-")[0];
    }

    public Plugin getPluginByName(String name) throws Exception{
        return this.plugin.getServer().getPluginManager().getPlugin(name);
    }
}
