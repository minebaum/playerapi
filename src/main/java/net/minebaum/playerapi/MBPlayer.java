package net.minebaum.playerapi;

import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.player.CloudPlayer;
import eu.thesimplecloud.api.player.text.CloudText;
import eu.thesimplecloud.module.permission.PermissionPool;
import eu.thesimplecloud.module.permission.player.IPermissionPlayer;
import lombok.Getter;
import net.minebaum.playerapi.coins.Coins;
import net.minebaum.playerapi.developement.logger.Log;
import net.minebaum.playerapi.events.PlayerSendToServerEvent;
import net.minebaum.playerapi.events.PropertieAddEvent;
import net.minebaum.playerapi.game.team.Team;
import net.minebaum.playerapi.properties.Propertie;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import java.util.Objects;

@Getter
public class MBPlayer {

    //FIELDS
    private Player spigotPlayer;
    private String name;
    private IPermissionPlayer permissionPlayer;
    private CloudPlayer cloudPlayer;
    private Propertie[] properties;
    private byte propertiesLength;
    private Team team;
    private Coins[] coinsList;
    private Log systemMessageSentLog;

    //CONSTRUCTOR NORM
    public MBPlayer(Player spigotPlayer){
        this.spigotPlayer = spigotPlayer;
        this.name = spigotPlayer.getName();
        this.permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(spigotPlayer.getUniqueId());
        this.cloudPlayer = (CloudPlayer) CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(spigotPlayer.getUniqueId());
        propertiesLength = 8;
        this.properties = new Propertie[propertiesLength];
        systemMessageSentLog = new Log();
    }

    //CONSTRUCTOR TODO
    public MBPlayer(Player spigotPlayer, Object placeHolderNullObject){
        this.spigotPlayer = spigotPlayer;
        this.name = spigotPlayer.getName();
        this.permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(spigotPlayer.getUniqueId());
        this.cloudPlayer = (CloudPlayer) CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(spigotPlayer.getUniqueId());
        propertiesLength = 8;
        this.properties = new Propertie[propertiesLength];
    }
    
    public MBPlayer(Player spigotPlayer, byte propertiesLength){
        this.spigotPlayer = spigotPlayer;
        this.name = spigotPlayer.getName();
        this.permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(spigotPlayer.getUniqueId());
        this.propertiesLength = propertiesLength;
        this.properties = new Propertie[propertiesLength];
    }

    public String getName() {
        return name;
    }

    public CloudPlayer getCloudPlayer() {
        return cloudPlayer;
    }

    public IPermissionPlayer getPermissionPlayer() {
        return permissionPlayer;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Coins[] getCoinsList() {
        return coinsList;
    }

    public void setCoinsList(Coins[] coinsList) {
        this.coinsList = coinsList;
    }

    public void setPropertiesLength(byte propertiesLength) {
        this.propertiesLength = propertiesLength;
    }

    public void setProperties(Propertie[] properties) {
        this.properties = properties;
    }

    public byte getPropertiesLength() {
        return propertiesLength;
    }

    public Propertie[] getProperties() {
        return properties;
    }

    public Player getSpigotPlayer() {
        return spigotPlayer;
    }

    public MBPlayer addPropertie(Propertie a){
        Bukkit.getPluginManager().callEvent(new PropertieAddEvent(a));
        if(propertiesLength > properties.length)
        properties[properties.length] = a;
        return this;
    }

    public MBPlayer setName(String a){
        name = a;
        return this;
    }

    public boolean hasPermission(String perm){
        return permissionPlayer.hasPermission(perm);
    }

    public boolean isInPermissionGroup(String name){
        return permissionPlayer.hasPermissionGroup(name);
    }

    public boolean hasPropertie(Propertie a){
        for(Propertie propertie : properties){
            if(propertie.equals(a)) return true;
        }
        return false;
    }

    public MBPlayer removePropertie(Propertie a) throws Exception{
        int i = 0;
        for(Propertie propertie : properties){
            if(a.equals(propertie)){
                properties[i] = null;
            }
            i++;
        }
        return this;
    }

    public enum MessageType{
        CHAT, ACTIONBAR, TITLE;
    }

    public MBPlayer send(String value, String value2, MessageType type){
        if(type.equals(MessageType.CHAT)){
            cloudPlayer.sendMessage(new CloudText(value));
            this.systemMessageSentLog.log("[CHAT] " + Objects.requireNonNull(value));
        }else if(type.equals(MessageType.TITLE)){
            cloudPlayer.sendTitle(Objects.requireNonNull(value), Objects.requireNonNull(value2), 5, 20, 5);
            this.systemMessageSentLog.log("[TITLE] " + Objects.requireNonNull(value));
            this.systemMessageSentLog.log("[TITLE] " + Objects.requireNonNull(value2));
        }else if(type.equals(MessageType.ACTIONBAR)){
            String message = value;
            if(message == null) message = "";
            cloudPlayer.sendActionBar(message.replaceAll("&", "§"));
            this.systemMessageSentLog.log("[ACTIONBAR] " + Objects.requireNonNull(message));
        }
        return this;
    }

    public MBPlayer sendMessage(String value){
        this.send(value, null, MessageType.CHAT);
        return this;
    }

    public MBPlayer sendActionbar(String value){
        this.send(value, null, MessageType.ACTIONBAR);
        return this;
    }

    public MBPlayer sendTitle(String value, String value2){
        this.send(value, value2, MessageType.TITLE);
        return this;
    }

    public PlayerInventory getInventory(){
        return this.spigotPlayer.getInventory();
    }
    
    public MBPlayer sendToServer(String serviceName){
        PlayerSendToServerEvent e = new PlayerSendToServerEvent(this);
        Bukkit.getPluginManager().callEvent(e);
        if(e.isCancelled()){
            return this;
        }else
        cloudPlayer.connect(Objects.requireNonNull(CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName(serviceName)));
        return this;
    }

    public PlayerConnection getConnection() {
        return ((CraftPlayer) spigotPlayer).getHandle().playerConnection;
    }

}